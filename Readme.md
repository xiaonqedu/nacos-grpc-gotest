- 运行命令安装包
```bash
go mod tidy
```
- 生成protobuf文件
```bash
cd pb
protoc --go_out=plugins=grpc:./ *.proto
```
- `保证nacos服务正常运行：`http://127.0.0.1:8848/nacos
- 运行服务端
```golang
// 启动 server8000
xiaonaiqiang1@ZBMac-C02CW08SM nacos-grpc-gotest % cd nacos-server8800
xiaonaiqiang1@ZBMac-C02CW08SM nacos-server8800 % go run nacos_server.go 
// 启动server8801
xiaonaiqiang1@ZBMac-C02CW08SM nacos-grpc-gotest % cd nacos-server8801
xiaonaiqiang1@ZBMac-C02CW08SM nacos-server8801 % go run nacos_server.go 
```

- 启动客户端测试负载均衡

```go
xiaonaiqiang1@ZBMac-C02CW08SM nacos-grpc-gotest % cd nacos-client 
// 运行第一次访问的是 127.0.0.1:8800
xiaonaiqiang1@ZBMac-C02CW08SM nacos-client % go run nacos_client.go 
127.0.0.1:8800
// 运行第一次访问的是 127.0.0.1:8801
xiaonaiqiang1@ZBMac-C02CW08SM nacos-client % go run nacos_client.go 
127.0.0.1:8801
```

- 查看nacos服务注册的状态

<img src="./assets/image-20211209104131395.png" style="width: 800px; margin-left: 20px;"> 

- 查看“服务详情”

<img src="./assets/image-20211209104251292.png" style="width: 800px; margin-left: 20px;"> 







